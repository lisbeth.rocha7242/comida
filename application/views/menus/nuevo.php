<h1 style="text-align: center;">Nuevo Registro</h1>
<br>

<form class="" action="index.html" method="post">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <br>
        <label for="">Cédula</label>
        <br>
        <input type="number" placeholder="Ingrese su cedula" class="form-control" name="cedula_cli" value="">
      </div>
      <br>
      <div class="col-md-4">
        <label for="">Primer Apellido</label>
        <br>
        <input type="text" placeholder="Ingrese su primer apellido" class="form-control" name="primer_apellido_cli" value="">
      </div>

      <div class="col-md-4">
        <label for="">Segundo Apellido</label>
        <br>
        <input type="text" placeholder="Ingrese su segundo apellido" class="form-control" name="segundo_apellido_cli" value="">
      </div>

    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <label for="">Nombres</label>
        <br>
        <input type="text" placeholder="Ingrese sus nombres" class="form-control" name="nombres_cli" value="">
      </div>

      <div class="col-md-8">
        <label for="">Dirección</label>
        <br>
        <input type="text" placeholder="Ingrese su direccion" class="form-control" name="direccion_cli" value="">
      </div>

    </div>

  </div>
  <br>
  <div class="col-md12 text-center">
    <button type="submit" name="button" class="btn btn-primary">Registrar</button>&nbsp &nbsp &nbsp ;
    <a href="<?php echo site_url();?>" class="btn btn-danger">Cancelar</a>

  </div>
</form>

<br>
<br>
