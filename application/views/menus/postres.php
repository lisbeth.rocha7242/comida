<div class="container-fluid bg-info text-white text-center">
  <h1 class="text-center" >POSTRES</h1>
  <div class="row">
  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/postre1.png" alt="..." width="350px" height="300px">
      <div class="caption">
        <h3>Postre de Fresa</h3>
        <p>Precio: $2,50</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/postre2.png" alt="..." width="365px" height="300px">
      <div class="caption">
        <h3>Postre de mango</h3>
        <p>Precio: $2,75</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/postre3.png" alt="..." width="310px" height="600px">
      <div class="caption">
        <h3>Postre sin azúcar</h3>
        <p>Precio: $3,00</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
<div class="row">
<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/postre4.png" alt="..." width="370px" height="300px">
    <div class="caption">
      <h3>Rebanada de chocolate</h3>
      <p>Precio: $2,50</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/postre5.png" alt="..." width="310px" height="300px">
    <div class="caption">
      <h3>Postre de Gelatina</h3>
      <p>Precio: $3,55</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/postre6.png" alt="..." width="290px" height="600px">
    <div class="caption">
      <h3>Postre Frances</h3>
      <p>Precio: $4,00</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>
</div>
</div>
