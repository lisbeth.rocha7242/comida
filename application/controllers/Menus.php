<?php
   class Menus extends CI_Controller
   {
     function __construct()
     {
       parent::__construct();
     }

     //Funcion que renderiza la vista Informacion
          public function nosotros(){
             $this->load->view('header');
             $this->load->view('menus/nosotros');
             $this->load->view('footer');
           }

           public function bebidas(){
              $this->load->view('header');
              $this->load->view('menus/bebidas');
              $this->load->view('footer');
            }
          public function postres(){
           $this->load->view('header');
           $this->load->view('menus/postres');
           $this->load->view('footer');
         }

         public function papas(){
           $this->load->view('header');
           $this->load->view('menus/papas');
           $this->load->view('footer');
         }


         public function hamburguesas(){
         $this->load->view('header');
         $this->load->view('menus/hamburguesas');
         $this->load->view('footer');
       }

       public function index(){
       $this->load->view('header');
       $this->load->view('menus/index');
       $this->load->view('footer');
     }

     public function nuevo(){
     $this->load->view('header');
     $this->load->view('menus/nuevo');
     $this->load->view('footer');
   }

   }
 ?>
