<div class="container-fluid bg-info text-white text-center">
  <h1 class="text-center" >PAPAS FRANCESAS</h1>
  <div class="row">
  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/papas1.png" alt="..." width="350px" height="300px">
      <div class="caption">
        <h3>Papas a la Francesa</h3>
        <p>Precio: $2,50</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/papas2.png" alt="..." width="310px" height="300px">
      <div class="caption">
        <h3>Papas con Queso</h3>
        <p>Precio: $3,00</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/papas3.png" alt="..." width="600px" height="600px">
      <div class="caption">
        <h3>Papas Picantes</h3>
        <p>Precio: $3,50</p>
        <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
<div class="row">
<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/papas4.png" alt="..." width="310px" height="300px">
    <div class="caption">
      <h3>Papas Crujientes</h3>
      <p>Precio: $2,00</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/papas5.png" alt="..." width="310px" height="300px">
    <div class="caption">
      <h3>Papas con Maicena</h3>
      <p>Precio: $3,75</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/images/papas6.png" alt="..." width="600px" height="600px">
    <div class="caption">
      <h3>Papas extra Picantes</h3>
      <p>Precio: $4,00</p>
      <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
    </div>
  </div>
</div>
</div>
</div>
