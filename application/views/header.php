<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>COMIDA</title>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVVrA8xbvFKLqEUqqYbZHiTuYMx6WL8b0&libraries=places&callback=initMap">
  </script>
  </head>
   <body>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo site_url(); ?>">BIENVENIDOS</a>
  </div>


  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo site_url(); ?>/menus/nosotros">Nosotros<span class="sr-only">(current)</span></a></li>
      <li><a href="<?php echo site_url(); ?>/menus/hamburguesas">Hamburguesas</a></li>
      <li><a href="<?php echo site_url(); ?>/menus/papas">Papas</a></li>
      <li><a href="<?php echo site_url(); ?>/menus/bebidas">Bebidas</a></li>
      <li><a href="<?php echo site_url(); ?>/menus/postres">Postres</a></li>

    </ul>

    <form class="navbar-form navbar-right">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="buscar">
      </div>
      <button type="submit" class="btn btn-default">Buscar</button>
    </form>

    <ul class="nav navbar-nav navbar-right">

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Registrarse<span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a href="<?php echo site_url(); ?>/menus/nuevo">Nuevo Registro</a></li>
        <li><a href="#">Listado de Registro</a></li>
      </li>
    </ul>


  </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  </nav>
</body>
